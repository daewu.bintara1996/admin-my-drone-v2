package com.satmei.adminmydrone.utils;

import android.content.Context;
import android.widget.Toast;

public class MyToast {
    Context context;
    public MyToast(Context context){
        this.context = context;
    }

    public void showShortToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showLongToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}
