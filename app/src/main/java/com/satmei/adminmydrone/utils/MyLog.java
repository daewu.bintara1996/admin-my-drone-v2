package com.satmei.adminmydrone.utils;

import android.util.Log;

public class MyLog {
    private static final String TAG = "MY-APP";

    public void d(String message){
        Log.d(TAG, message);
    }

}
