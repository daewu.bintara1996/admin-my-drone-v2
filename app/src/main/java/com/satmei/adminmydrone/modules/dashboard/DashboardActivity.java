package com.satmei.adminmydrone.modules.dashboard;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.satmei.adminmydrone.R;
import com.satmei.adminmydrone.base.BaseActivity;
import com.satmei.adminmydrone.modules.login.LoginActivity;
import com.satmei.adminmydrone.modules.splash.SplashActivity;

public class DashboardActivity extends BaseActivity{

    TextView tvAx, tvAy, tvAz, tvGx, tvGy, tvGz, tvMFx, tvMFy, tvMFz, tvEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        tvAx = findViewById(R.id.tvAx);
        tvAy = findViewById(R.id.tvAy);
        tvAz = findViewById(R.id.tvAz);
        tvGx = findViewById(R.id.tvGx);
        tvGy = findViewById(R.id.tvGy);
        tvGz = findViewById(R.id.tvGz);
        tvMFx = findViewById(R.id.tvMFx);
        tvMFy = findViewById(R.id.tvMFy);
        tvMFz = findViewById(R.id.tvMFz);
        tvEmail = findViewById(R.id.tvEmail);

        tvAx.setText("X : ");
        tvAy.setText("Y : ");
        tvAz.setText("Z : ");
        tvGx.setText("X : ");
        tvGy.setText("Y : ");
        tvGz.setText("Z : ");
        tvMFx.setText("X : ");
        tvMFy.setText("Y : ");
        tvMFz.setText("Z : ");

        tvEmail.setText(myPreferences.getPreferencesString(TOKEN_EMAIL_KEY));

        onRecivedSensorDatas();


    }

    private void onRecivedSensorDatas() {
        String USER_ID = myPreferences.getPreferencesString(TOKEN_USER_ID_KEY);

        if (USER_ID == null){
            myPreferences.clearAllPreferences();
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        myRef = database.getReference("user-sensors");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    tvAx.setText("X : " + dataSnapshot.child(USER_ID).child("accelerometer").child("X").getValue().toString());
                    tvAy.setText("Y : " + dataSnapshot.child(USER_ID).child("accelerometer").child("Y").getValue().toString());
                    tvAz.setText("Z : " + dataSnapshot.child(USER_ID).child("accelerometer").child("Z").getValue().toString());

                    tvGx.setText("X : " + dataSnapshot.child(USER_ID).child("gyroscope").child("X").getValue().toString());
                    tvGy.setText("Y : " + dataSnapshot.child(USER_ID).child("gyroscope").child("Y").getValue().toString());
                    tvGz.setText("Z : " + dataSnapshot.child(USER_ID).child("gyroscope").child("Z").getValue().toString());

                    tvMFx.setText("X : " + dataSnapshot.child(USER_ID).child("magneticField").child("X").getValue().toString());
                    tvMFy.setText("Y : " + dataSnapshot.child(USER_ID).child("magneticField").child("Y").getValue().toString());
                    tvMFz.setText("Z : " + dataSnapshot.child(USER_ID).child("magneticField").child("Z").getValue().toString());
                } catch (Exception e){
                    myRef.removeEventListener(this);
                    alertDataNotFound();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
            }
        });
    }

    private void alertDataNotFound() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Data tidak ada");
        builder.setMessage("Pastikan Aplikasi MyDrone login dengan akun yang sama");
        builder.setPositiveButton("Refresh", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                onRecivedSensorDatas();
            }
        });
        builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                finish();

            }
        });
        builder.setNeutralButton("Logout", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                onLogounClick();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void onLogounClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Keluar Akun");
        builder.setMessage("Yakin akan keluar dari akun ini?");
        builder.setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                myPreferences.clearAllPreferences();
                myPreferences.clearAllPreferences();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void btnLogout(View view) {
        onLogounClick();
    }


}