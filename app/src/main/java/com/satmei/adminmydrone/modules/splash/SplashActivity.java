package com.satmei.adminmydrone.modules.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.satmei.adminmydrone.R;
import com.satmei.adminmydrone.base.BaseActivity;
import com.satmei.adminmydrone.modules.dashboard.DashboardActivity;
import com.satmei.adminmydrone.modules.login.LoginActivity;

public class SplashActivity extends BaseActivity {
    Handler handler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkPreferences();
            }
        }, 1000);
    }

    private void checkPreferences() {
        if (myPreferences.getPreferencesString(TOKEN_EMAIL_KEY) == null) {
            myToast.showShortToast("Login untuk memulai");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            myToast.showShortToast("Selamat datang kembali");
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        handler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}